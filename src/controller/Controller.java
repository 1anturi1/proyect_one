package controller;

import api.ITaxiTripsManager;

import model.Logic.TaxiTripsManager;
import model.data_structures.Lista;
import model.data_structures.Pila;
import model.data_structures.Cola;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.LinkedList;
import VO.Taxi;
import VO.ZonaServicios;
import VO.Compania;
import VO.CompaniaServicios;
import VO.CompaniaTaxi;
import VO.InfoTaxiRango;
import VO.RangoDistancia;
import VO.RangoFechaHora;
import VO.Service;
import VO.Servicio;
import VO.ServiciosValorPagado;

public class Controller 
{

	/**
	 * modela el manejador de la clase l�gica
	 */
	private static ITaxiTripsManager manager = new TaxiTripsManager();

	public static TaxiTripsManager getManager()
	{
		return (TaxiTripsManager) manager ;
	}
	
	//1C
	public static boolean cargarSistema(String direccionJson)
	{
		return manager.loadServices(direccionJson);
	}
	//A1
	public static Cola<Service> darServiciosEnRango(RangoFechaHora rango)
	{
		return manager.darServiciosEnPeriodo(rango);
	}

	//2A
	public static Taxi darTaxiConMasServiciosEnCompaniaYRango(RangoFechaHora rango, String company)
	{
		return manager.darTaxiConMasServiciosEnCompaniaYRango(rango, company);
	}

	//3A
	public static InfoTaxiRango darInformacionTaxiEnRango(String id, RangoFechaHora rango)
	{
		return manager.darInformacionTaxiEnRango(id, rango);
	}

	//4A
	public static Lista<RangoDistancia> darListaRangosDistancia(String fecha, String horaInicial, String horaFinal) 
	{
		return manager.darListaRangosDistancia(fecha, horaInicial, horaFinal);
	}
	
	//1B
	public static LinkedList<Compania> darCompaniasTaxisInscritos()
	{
		return manager.darCompaniasTaxisInscritos();
	}
	
	//2B
	public static Taxi darTaxiMayorFacturacion(RangoFechaHora rango, String nomCompania)
	{
		return manager.darTaxiMayorFacturacion(rango, nomCompania);
	}

	
	//3B
	public static ServiciosValorPagado darServiciosZonaValorTotal(RangoFechaHora rango, String idZona)
	{
		return manager.darServiciosZonaValorTotal(rango, idZona);
	}
	
	//4B

	public static LinkedList<ZonaServicios> darZonasServicios(RangoFechaHora rango)
	{
		return manager.darZonasServicios(rango);

	}

	public static void loadServicesMediano( ) 
	{
		// To define the dataset file's name 
		String serviceFile = "./data/taxi-trips-wrvz-psew-subset-medium.json";
		
		manager.loadServices( serviceFile );
	}

	public static Lista<Taxi> getTaxisOfCompany (String company) 
	{
		return null ;
	}
	
	public static void inicializarLista()
	{
		manager.inicializarLista();
	}
	

	//2C
	public static LinkedList<CompaniaServicios> companiasMasServicios(RangoFechaHora rango, int n)
	{
		return manager.companiasMasServicios(rango, n);
	}

	//3C
	public static LinkedList<CompaniaTaxi> taxisMasRentables()
	{
		return manager.taxisMasRentables();
	}

	//4C
	public static IStack <Service> darServicioResumen(String taxiId, String horaInicial, String horaFinal, String fecha)
	{
		return manager.darServicioResumen(taxiId,horaInicial,horaFinal,fecha);
	}



}
