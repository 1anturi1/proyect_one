package VO;

import model.data_structures.Lista;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>
{

	private String company ;
	private String taxiId ;
	private Lista<Service> servicios ;

	public Taxi (String pCompany, String pTaxiId)
	{
		company =pCompany ;
		taxiId = pTaxiId ;
		servicios = new Lista<Service>();
	}
	
	
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId() 
	{
		return taxiId;
	}

	
	/**
	 * @return company
	 */
	public String getCompany() 
	{
		return company;
	}
	
	public Lista<Service> getServicios()
	{
		return servicios ;
	}
	
	@Override
	public int compareTo(Taxi o) 
	{
		int variable = 0;

		if(taxiId.compareTo(o.taxiId) > 0)
		{
			variable =1 ;
		}

		else if(taxiId.compareTo(o.taxiId) < 0)
		{
			variable = -1 ;
		}

		return variable ;
	}	
}