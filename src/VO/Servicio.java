package VO;

/**
 * Representation of a Service object
 */
public class Servicio implements Comparable<Servicio>
{	
	
	
	private String tripId ;
	
	private String taxiId ;
	
	private int tripSeconds ;
	
	private double tripMiles ;
	
	private double tripTotal ;
	
	public Servicio(String pTripId, String pTaxiId, int pTripSeconds, double pTripMiles, double pTripTotal)
	{
		tripId = pTripId;
		taxiId = pTaxiId ;
		tripSeconds = pTripSeconds ;
		tripMiles = pTripMiles;
		tripTotal = pTripTotal ;
		
		
	}
	
	
	
	
	public void setTripId(String tripId) {
		this.tripId = tripId;
	}




	public void setTaxiId(String taxiId) {
		this.taxiId = taxiId;
	}




	public void setTripSeconds(int tripSeconds) {
		this.tripSeconds = tripSeconds;
	}




	public void setTripMiles(double tripMiles) {
		this.tripMiles = tripMiles;
	}




	public void setTripTotal(double tripTotal) {
		this.tripTotal = tripTotal;
	}




	/**
	 * @return id - Trip_id
	 */
	public String getTripId()
	{
		// TODO Auto-generated method stub
		return tripId;
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() 
	{
		// TODO Auto-generated method stub
		return taxiId;
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() 
	{
		// TODO Auto-generated method stub
		return tripSeconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() 
	{
		// TODO Auto-generated method stub
		return tripMiles;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() 
	{
		// TODO Auto-generated method stub
		return tripTotal;
	}

	@Override
	public int compareTo(Servicio o) 
	{		
		return tripId.compareTo(o.getTripId()) ;
	}
}
