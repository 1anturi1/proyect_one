package VO;

import model.data_structures.LinkedList;
import model.data_structures.Lista;
import VO.RangoFechaHora;
import VO.Servicio;

/**
 * VO utilizado para req 3A, contiene el rango en el que se pidi� generar la informaci�n del taxi y 
 * los datos solicitados
 */
public class InfoTaxiRango 
{
	//ATRIBUTOS
	
	/**
	 * Modela el id del taxi 
	 */
	private String idTaxi;
	
	/**
	 * Modela el rango de fechas y horas (iniciales y finales)
	 */
	private RangoFechaHora rango;
	
	/**
	 * modela el nombre de la compa�ia del taxi
	 */
	private String company;
	
	/**
	 * modela el dinero que gano el taxi en el rango
	 */
	private double plataGanada;
	
	/**
	 * Modela la lista de servicios que presto el taxi en el rango
	 */
	private Lista<Service> serviciosPrestadosEnRango; 
	
	/**
	 * modela la distancia recorrida por el taxi en el rango
	 */
	private double distanciaTotalRecorrida; 
	
	/**
	 * modela el tiempo total de servicios
	 */
	private String tiempoTotal;

	public InfoTaxiRango(String pTaxiId, RangoFechaHora pRango, String pCompany, Lista<Service> pLista)
	{
		
		idTaxi = pTaxiId ;
		rango = pRango;
		company = pCompany ;
		plataGanada = 0;
		serviciosPrestadosEnRango = pLista ;
		distanciaTotalRecorrida = 0 ;
		
		
	}
	
	
	//M�TODOS
	
	/**
	 * @return the idTaxi
	 */
	public String getIdTaxi()
	{
		return idTaxi;
	}

	/**
	 * @param idTaxi the idTaxi to set
	 */
	public void setIdTaxi(String idTaxi)
	{
		this.idTaxi = idTaxi;
	}

	/**
	 * @return the rango
	 */
	public RangoFechaHora getRango()
	{
		return rango;
	}

	/**
	 * @param rango the rango to set
	 */
	public void setRango(RangoFechaHora rango)
	{
		this.rango = rango;
	}

	/**
	 * @return the company
	 */
	public String getCompany()
	{
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(String company) 
	{
		this.company = company;
	}

	/**
	 * @return the plataGanada
	 */
	public double getPlataGanada() 
	{
		if(serviciosPrestadosEnRango !=null)
		{
		serviciosPrestadosEnRango.listing();
		while(serviciosPrestadosEnRango.next())
		{
			plataGanada += serviciosPrestadosEnRango.getCurrent().getTripTotal();
			serviciosPrestadosEnRango.avanzar();
		}
		}
		
		return plataGanada;
	}

	/**
	 * @return the serviciosPrestadosEnRango
	 */
	public Lista<Service> getServiciosPrestadosEnRango()
	{
		return serviciosPrestadosEnRango;
	}

	/**
	 * @param serviciosPrestadosEnRango the serviciosPrestadosEnRango to set
	 */

	/**
	 * @return the distanciaTotalRecorrida
	 */
	public double getDistanciaTotalRecorrida()
	{
		if(serviciosPrestadosEnRango !=null)
		{
		serviciosPrestadosEnRango.listing();
		while(serviciosPrestadosEnRango.next())
		{
			distanciaTotalRecorrida += serviciosPrestadosEnRango.getCurrent().getTripMiles();
			serviciosPrestadosEnRango.avanzar();
		}
		}
		
		return distanciaTotalRecorrida;
	}

	

	/**
	 * @return the tiempoTotal
	 */
	public String getTiempoTotal()
	{
		
		return tiempoTotal;
	}

	/**
	 * @param tiempoTotal the tiempoTotal to set
	 */
	public void setTiempoTotal(String tiempoTotal) 
	{
		this.tiempoTotal = tiempoTotal;
	}
	
}
