package VO;

import model.data_structures.LinkedList;

public class ZonaServicios implements Comparable<ZonaServicios>{

	private int idZona;
	
	private LinkedList<FechaServicios> fechasServicios;
	
	public ZonaServicios(int pZOna)
	{
		idZona=pZOna;
	}
	

	public int getIdZona() {
		return idZona;
	}



	public void setIdZona(int idZona) {
		this.idZona = idZona;
	}



	public LinkedList<FechaServicios> getFechasServicios() {
		return fechasServicios;
	}



	public void setFechasServicios(LinkedList<FechaServicios> fechasServicios) {
		this.fechasServicios = fechasServicios;
	}



	@Override
	public int compareTo(ZonaServicios o) 
	{
		int resu=0;
		if(idZona<o.idZona)
		{
			resu=-1;
		}
		else if(idZona >o.idZona)
		{
			resu = 1;
		}
		// TODO Auto-generated method stub
		
		return resu;
	}
}
