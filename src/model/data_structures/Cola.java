package model.data_structures;

public class Cola <T extends Comparable <T>> implements IQueue<T>
{

	private Node<T> primero ;
	private Node<T> ultimo ;
	private int tamano ;
	
	public Cola()
	{
		primero = null ;
		ultimo = null ;
		tamano = 0 ;
		
	}
	
	
	
	@Override
	public void enqueue(T item) 
	{
		if (ultimo== null)
		{
			ultimo =new Node<T>(item) ;
			primero = ultimo ;
			tamano++ ;
		}
		else 
		{
			Node<T> vari = new Node<T>(item) ;
			
			ultimo.cambiarSig(vari);
			
			ultimo = vari ;
			
			tamano++ ;
			
		}
		
	}

	@Override
	public T dequeue() 
	{
		if(primero !=null)
		{
			T vari = primero.darT() ;
			
			primero = primero.darSig();
			tamano--;
			return vari ;
		}
		else
		return null;
	}

	@Override
	public boolean isEmpty() 
	{
		return primero ==null?false:true;
	}



	@Override
	public int size() 
	{
		return tamano;
	}

}

