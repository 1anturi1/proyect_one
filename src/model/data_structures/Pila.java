package model.data_structures;

public class Pila<T extends Comparable <T>> implements IStack<T>
{

	private Node<T> tope ;
	private int tama ;

	public Pila()
	{
		tope = null ;
		tama = 0 ;
	}

	@Override
	public void push(T item) 
	{
		if (tope ==null)
		{
			tope = new Node<T>(item) ;
			tama++;

		}
		else
		{
			Node<T> nuevo = new Node<T>(item);
			nuevo.cambiarSig(tope);
			tope = nuevo;
			tama++ ;

		}

	}

	@Override
	public T pop() 
	{
		if ( tope != null )
		{
			tama-- ;
			T vari = tope.darT() ;
			tope = tope.darSig() ;
			return vari ;
		}
		else return null;
	}

	@Override
	public boolean isEmpty() 
	{
		return tope ==null?false:true;
	}

}