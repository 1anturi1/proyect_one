package model.data_structures;

import VO.Taxi;

public class Lista<T extends Comparable <T>> implements LinkedList<T>
{

	private Node<T> cabeza ;

	private Node<T> actual ;

	private Node<T> ultimo ;

	private int size ;

	public Lista()
	{
		cabeza = null ;

		actual= cabeza ;

		ultimo = cabeza ;

		size = 0 ;

	}


	@Override
	public void add(T bag)
	{
		if(cabeza == null )
		{
			cabeza = new Node(bag) ;
			ultimo = cabeza ;
		}
		else 
		{
			Node<T> nuevo = new Node<T>(bag) ;


			ultimo.cambiarSig(nuevo);
			ultimo = nuevo ;

		}
		size++;
	}


	@Override
	public void delete(T bag) 
	{
		if(bag!=null)
		{
			if(cabeza != null )
			{
				boolean vari = true ;
				if(cabeza.darT().compareTo(bag)==0)
				{
					cabeza = cabeza.darSig() ;
					size-- ;
				}
				else if(ultimo.darT().compareTo(bag)==0)
				{
					listing();
					Node<T> aux=cabeza;
					while(true)
					{
						if(actual.darSig().darT().compareTo(bag)==0)
						{
							ultimo=actual;
							ultimo.cambiarSig(null);
							break;
						}
						else
							avanzar();
					}
					size-- ;
				}
				else
				{
					listing();
					while (vari)
					{

						if(actual.darSig().darT().equals(bag))
						{
							actual.cambiarSig(actual.darSig().darSig());
							size-- ;
							vari = false;
						}
						else if(actual == null)
						{
							vari = false ;
						}
						else
							avanzar();

					}
				}

			}
		}
	}


	@Override
	public T getElement( T bag) 
	{
		T result = null;

		if(cabeza!=null)
		{
			listing();
			while(true)
			{

				if(actual.darT().compareTo(bag)==0)
				{
					result = getCurrent() ;
					break;
				}
				else
				{
					if(next())
					{
						avanzar();
					}
					else
					{
						break;
					}
				}
			}

		}
		return result;
	}



	@Override
	public int size() 
	{
		return size ;
	}



	@Override
	public void listing() 
	{
		actual = cabeza ;

	}


	@Override
	public boolean next() 
	{
		return actual.darSig() != null? true : false;

	}

	public void avanzar()
	{
		actual = actual.darSig() ;
	}


	public Node<T> getUltimo() 
	{
		return ultimo;
	}


	@Override
	public T get(T bag) 
	{
		return null ;
	}


	@Override
	public T getCurrent() 
	{
		return actual.darT();
	}
	public Node<T> getActual()
	{
		return actual ;
	}
	
	public void combinarLista(Lista<T> pLista)
	{
		ultimo.cambiarSig(pLista.getCabeza());
	}
	
	public Node<T> getCabeza()
	{
		return cabeza ;
	} 
}
