package model.Logic;

import VO.Taxi;

import VO.ZonaServicios;
import api.ITaxiTripsManager;
import model.data_structures.Lista;
import model.data_structures.Pila;
import model.data_structures.Cola;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.LinkedList;
import VO.Compania;
import VO.CompaniaServicios;
import VO.CompaniaTaxi;
import VO.InfoTaxiRango;
import VO.RangoDistancia;
import VO.RangoFechaHora;
import VO.Service;
import VO.Servicio;
import VO.ServiciosValorPagado;

import java.awt.List;
import java.awt.Point;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.sun.glass.ui.Pixels.Format;
import com.sun.org.apache.xml.internal.serialize.LineSeparator;

public class TaxiTripsManager implements ITaxiTripsManager 
{

	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-0";


	private static Lista<Compania> listCompania;

	private static Lista<ZonaServicios> listZonaServicios;

	public void inicializarLista()
	{
		listCompania = new Lista<Compania>();
	}

	@Override //1C
	public boolean loadServices(String serviceFile) 
	{
		listZonaServicios = new Lista<ZonaServicios>();
		JsonParser parser = new JsonParser();

		try 
		{
			String taxiTripsDatos = serviceFile;

			/* Cargar todos los JsonObject (servicio) definidos en un JsonArray en el archivo */

			JsonArray arr= (JsonArray) parser.parse(new FileReader(taxiTripsDatos));

			/* Tratar cada JsonObject (Servicio taxi) del JsonArray */
			for (int i = 0; arr != null && i < arr.size(); i++)
			{
				JsonObject obj= (JsonObject) arr.get(i);
				//------------------------------------------
				String tripId = "";

				if ( obj.get("trip_id") != null )
				{ 
					tripId = obj.get("trip_id").getAsString(); 
				}
				//------------------------------------------
				String taxiId = "";
				if ( obj.get("taxi_id") != null )
				{ 
					taxiId = obj.get("taxi_id").getAsString(); 
				}
				//------------------------------------------
				String fechaini = "Nan";
				String horaini = "NaN" ;

				if ( obj.get("trip_start_timestamp") != null )
				{ 
					String a = (obj.get("trip_start_timestamp").getAsString());

					String [] b = a.split("T");
					fechaini = b[0] ;
					horaini = b[1] ;
				}

				//------------------------------------------
				String fechafin ="NaN" ;
				String horafin	="NaN" ;	

				if ( obj.get("trip_end_timestamp") != null )
				{ 
					String a = (obj.get("trip_end_timestamp").getAsString());
					String [] b = a.split("T");
					fechafin = b[0] ;
					horafin = b[1] ;

				}
				//------------------------------------------
				int tripSeconds = 0;
				if ( obj.get("trip_seconds") != null )
				{ 
					tripSeconds = obj.get("trip_seconds").getAsInt(); 
				}
				//------------------------------------------
				float tripMiles = 0 ;				
				if ( obj.get("trip_miles") != null )
				{ 
					tripMiles = obj.get("trip_miles").getAsFloat(); 
				}
				//------------------------------------------
				String pickupCensusTrack = "" ;				
				if ( obj.get("pickup_census_tract") != null )
				{ 
					pickupCensusTrack = obj.get("pickup_census_tract").getAsString(); 
				}
				//------------------------------------------
				String dropoffCensusTract = "" ;				
				if ( obj.get("dropoff_census_tract") != null )
				{ 
					dropoffCensusTract = obj.get("dropoff_census_tract").getAsString(); 
				}
				//------------------------------------------
				int pickupCommunityArea = 0;
				if ( obj.get("pickup_community_area") != null )
				{ 
					pickupCommunityArea = obj.get("pickup_community_area").getAsInt(); 
				}
				//------------------------------------------
				int dropoffCommunityArea = 0;
				if ( obj.get("dropoff_community_area") != null )
				{ 
					dropoffCommunityArea = obj.get("dropoff_community_area").getAsInt(); 
				}
				//------------------------------------------
				float fare = 0;
				if ( obj.get("fare") != null )
				{ 
					fare = obj.get("fare").getAsFloat(); 
				}
				//------------------------------------------
				float tips = 0;
				if ( obj.get("tips") != null )
				{ 
					tips = obj.get("tips").getAsFloat(); 
				}

				//------------------------------------------
				float tolls = 0;
				if ( obj.get("tolls") != null )
				{ 
					tolls = obj.get("tolls").getAsFloat(); 
				}
				//------------------------------------------
				float extras = 0;
				if ( obj.get("extras") != null )
				{ 
					extras = obj.get("extras").getAsFloat(); 
				}
				//------------------------------------------
				float tripTotal =0 ;
				if ( obj.get("trip_total") != null )
				{ 
					tripTotal = obj.get("trip_total").getAsFloat(); 
				}

				//------------------------------------------
				String paymentType = "" ;				
				if ( obj.get("payment_type") != null )
				{ 
					paymentType = obj.get("payment_type").getAsString(); 
				}
				//------------------------------------------
				String company = "Independent";
				if ( obj.get("company") != null )
				{ 
					company = obj.get("company").getAsString(); 
				}
				//------------------------------------------
				double pickupCentroidLatitude =0 ;
				if ( obj.get("pickup_centroid_latitude") != null )
				{ 
					pickupCentroidLatitude = obj.get("pickup_centroid_latitude").getAsDouble(); 
				}
				//------------------------------------------
				double pickupCentroidLongitude =0 ;
				if ( obj.get("pickup_centroid_longitude") != null )
				{ 
					pickupCentroidLongitude = obj.get("pickup_centroid_longitude").getAsDouble(); 
				}
				//------------------------------------------
				Point pickupCentroidLocation = null;		

				if ( obj.get("pickup_centroid_location") != null )
				{ 
					JsonObject a = obj.get("pickup_centroid_location").getAsJsonObject();
					JsonArray b = a.get("coordinates").getAsJsonArray();
					int x = b.get(0).getAsInt();
					int y = b.get(1).getAsInt();

					pickupCentroidLocation = new Point(x, y) ;

				}
				//------------------------------------------
				double dropoffCentroidLatitude =0 ;
				if ( obj.get("dropoff_centroid_latitude") != null )
				{ 
					dropoffCentroidLatitude = obj.get("dropoff_centroid_latitude").getAsDouble(); 
				}
				//------------------------------------------
				double dropoffCentroidLongitude =0 ;
				if ( obj.get("dropoff_centroid_longitude") != null )
				{ 
					dropoffCentroidLongitude = obj.get("dropoff_centroid_longitude").getAsDouble(); 
				}
				//------------------------------------------
				Point dropoffCentroidLocation = null ;
				if ( obj.get("dropoff_centroid_location") != null )
				{ 

					JsonObject a = obj.get("dropoff_centroid_location").getAsJsonObject(); 
					JsonArray b = a.get("coordinates").getAsJsonArray();
					int x = b.get(0).getAsInt();
					int y = b.get(1).getAsInt();

					dropoffCentroidLocation = new Point(x, y) ;

				}


				//--------------------------------------------

				Service ser = new Service(tripId, taxiId, fechaini, horaini, fechafin, horafin,
						tripSeconds, tripMiles, pickupCensusTrack, dropoffCensusTract,
						pickupCommunityArea, dropoffCommunityArea,
						fare, tips, tolls, extras, tripTotal, paymentType, company,
						pickupCentroidLatitude, pickupCentroidLongitude, pickupCentroidLocation, 
						dropoffCentroidLongitude,dropoffCentroidLatitude, dropoffCentroidLocation);



				//--------------------------------------------------

				Taxi tax = new Taxi(company, taxiId);	

				//--------------------------------------------------

				Compania com = new Compania();
				com.setNombre(company);

				if(listCompania.getElement(com)==null)
				{
					listCompania.add(com);
					com.setTaxisInscritos(new Lista<Taxi>());
					com.getTaxisInscritos().add(tax);
					tax.getServicios().add(ser);
				}
				else 
				{

					if(listCompania.getElement(com).getTaxisInscritos().getElement(tax) == null)
					{
						listCompania.getElement(com).getTaxisInscritos().add(tax);
						listCompania.getElement(com).getTaxisInscritos().getElement(tax).getServicios().add(ser);
					}
					else
					{
						listCompania.getElement(com).getTaxisInscritos().getElement(tax).getServicios().add(ser);
					}


				}
				ZonaServicios zona= new ZonaServicios(dropoffCommunityArea);

				ZonaServicios zon= new ZonaServicios(pickupCommunityArea);

				if(listZonaServicios.getElement(zona)==null)
				{
					listZonaServicios.add(zona);
				}
				if(listZonaServicios.getElement(zon)==null)
				{
					listZonaServicios.add(zon);
				}

			}
			System.out.println(listCompania.size());

		}

		catch (JsonIOException e1 ) 
		{
			e1.printStackTrace();
		}
		catch (JsonSyntaxException e2) 
		{


			e2.printStackTrace();
		}
		catch (FileNotFoundException e3) 
		{

			e3.printStackTrace();
		}



		System.out.println("Inside loadServices with " + serviceFile);


		return false ;
	}


	@Override //1A
	public Cola<Service> darServiciosEnPeriodo(RangoFechaHora rango)
	{
		String fechini= rango.getFechaInicial() ;
		String horaini= rango.getHoraInicio() ;
		String fechfin= rango.getFechaFinal() ;
		String horafin= rango.getHoraFinal() ;
		Cola<Service> nueva = new Cola<Service>() ;
		Lista<Service> aux = new Lista<Service>() ;


		if (listCompania!=null)
		{
			listCompania.listing();
			while(listCompania.getActual() !=null)
			{

				Compania actual = listCompania.getCurrent();
				actual.getTaxisInscritos().listing();
				while(actual.getTaxisInscritos().next())
				{
					Taxi taxactual = actual.getTaxisInscritos().getCurrent() ;
					taxactual.getServicios().listing();
					while(taxactual.getServicios().next())
					{
						Service seractual = taxactual.getServicios().getCurrent() ;
						if(seractual.getFechaInicial().compareTo(fechini) >= 0 && seractual.getHoraInicial().compareTo(horaini) >= 0
								&& seractual.getFechaFinal().compareTo(fechfin) <= 0 && seractual.getHoraFinal().compareTo(horafin) <= 0)
						{
							aux.add(seractual);
						}
						taxactual.getServicios().avanzar();
					}
					actual.getTaxisInscritos().avanzar();
				}

				listCompania.avanzar();
			}

		}


		boolean centi = true ;
		while(centi)
		{	
			aux.listing(); 
			Service compa = aux.getCabeza().darT() ;
			while(true)
			{
				Service actu = aux.getCurrent() ;
				if(actu.getHoraFinal().compareTo(compa.getHoraFinal()) < 0)
				{
					compa = actu;
				}
				aux.avanzar();

				if(aux.next() ==false)
					break;
			}
			nueva.enqueue(compa);
			aux.delete(compa);
			if(aux.size()== 1)
			{
				centi = false ;
				nueva.enqueue(aux.getCurrent());
				aux.delete(aux.getCurrent());
			}

		}

		return nueva ;
	}

	@Override //2A
	public Taxi darTaxiConMasServiciosEnCompaniaYRango(RangoFechaHora rango, String company)
	{

		String fechini= rango.getFechaInicial() ;
		String horaini= rango.getHoraInicio() ;
		String fechfin= rango.getFechaFinal() ;
		String horafin= rango.getHoraFinal() ;		
		listCompania.listing();
		Compania compa = null;

		while(true)
		{
			if(listCompania.getCurrent().getNombre().equalsIgnoreCase(company))
			{
				compa= listCompania.getCurrent() ;
				break;
			}
			listCompania.avanzar();
		}

		int comparable = 0 ;
		Taxi elMas = null ;

		while(compa.getTaxisInscritos().next())
		{
			Taxi taxactual = compa.getTaxisInscritos().getCurrent() ;
			taxactual.getServicios().listing();
			int num= 0 ;

			while(taxactual.getServicios().next())
			{ 

				Service seractual = taxactual.getServicios().getCurrent() ;

				if(seractual.getFechaInicial().compareTo(fechini) >= 0 && seractual.getHoraInicial().compareTo(horaini) >= 0
						&& seractual.getFechaFinal().compareTo(fechfin) <= 0 && seractual.getHoraFinal().compareTo(horafin) <= 0)
				{
					num++;
				}
				taxactual.getServicios().avanzar();
			}

			if(num >comparable)
			{
				comparable =num;
				elMas = taxactual ;
			}
		}
		return elMas;
	}

	@Override //3A
	public InfoTaxiRango darInformacionTaxiEnRango(String id, RangoFechaHora rango)
	{
		String fechini= rango.getFechaInicial() ;
		String horaini= rango.getHoraInicio() ;
		String fechfin= rango.getFechaFinal() ;
		String horafin= rango.getHoraFinal() ;	

		Taxi seleccionado = null ;
		Compania  sel = null ;

		Lista<Service> aux = new Lista<Service>() ;

		listCompania.listing();
		boolean centi = true ;
		while(centi)
		{
			listCompania.getCurrent().getTaxisInscritos().listing(); 
			while(listCompania.getCurrent().getTaxisInscritos().next())
			{
				Taxi actual = listCompania.getCurrent().getTaxisInscritos().getCurrent() ;

				if(actual.getTaxiId().equals(id))
				{
					seleccionado = actual ;
					sel = listCompania.getCurrent();
					centi=false ;
					break;
				}
				listCompania.getCurrent().getTaxisInscritos().avanzar();
			}
			listCompania.avanzar();
		}

		seleccionado.getServicios().listing();
		while(seleccionado.getServicios().next())
		{ 
			Service seractual = seleccionado.getServicios().getCurrent() ;

			if(seractual.getFechaInicial().compareTo(fechini) >= 0 && seractual.getHoraInicial().compareTo(horaini) >= 0
					&& seractual.getFechaFinal().compareTo(fechfin) <= 0 && seractual.getHoraFinal().compareTo(horafin) <= 0)
			{
				aux.add(seractual);
			}
			seleccionado.getServicios().avanzar();
		}

		InfoTaxiRango resul = new InfoTaxiRango(seleccionado.getTaxiId(), rango,sel.getNombre() , aux) ;


		return resul;
	}

	@Override //4A
	public Lista<RangoDistancia> darListaRangosDistancia(String fecha, String horaInicial, String horaFinal) 
	{
		Lista<RangoDistancia> retor = new Lista<RangoDistancia>() ;
		Lista<Service> aux = new Lista<Service>() ;

		if (listCompania!=null)
		{
			listCompania.listing();
			while(listCompania.next())
			{
				Compania actual = listCompania.getCurrent();
				actual.getTaxisInscritos().listing();
				while(actual.getTaxisInscritos().next())
				{
					Taxi taxactual = actual.getTaxisInscritos().getCurrent() ;
					taxactual.getServicios().listing();
					while(taxactual.getServicios().next())
					{
						Service seractual = taxactual.getServicios().getCurrent() ;
						if(seractual.getFechaInicial().compareTo(fecha) == 0 && seractual.getHoraInicial().compareTo(horaInicial) >= 0
								&& seractual.getHoraFinal().compareTo(horaFinal) <= 0)
						{
							aux.add(seractual);
						}
						taxactual.getServicios().avanzar();
					}
					actual.getTaxisInscritos().avanzar();
				}

				listCompania.avanzar();
			}

		}
		boolean centi = true ;


		double i = 0.0 ;
		double j = 1.0 ;

		while(centi)
		{

			retor.add(new RangoDistancia(j, i));
			RangoDistancia rangoActual = retor.getUltimo().darT() ;
			aux.listing(); 
			while(aux.next())
			{
				Service actu = aux.getCurrent() ;
				if(actu.getTripMiles() >= i && actu.getTripMiles() < j )
				{
					aux.delete(actu);
					rangoActual.getServiciosEnRango().add(actu);
				}
				aux.avanzar();
			}
			i+=1;
			j+=1 ;
			if(aux.size()== 1)
			{
				centi = false ;
			}


		}
		retor.add(new RangoDistancia(j, i));
		retor.getUltimo().darT().getServiciosEnRango().add(aux.getCurrent());
		aux.delete(aux.getCurrent());
		aux= null ;

		return retor;
	}
	public Compania darMenor(Lista<Compania> pLista)
	{
		
		pLista.listing();
		Compania aux=pLista.getCurrent();
		while(true)
		{
			Compania d = pLista.getCurrent();
			if(d.getNombre().compareTo(aux.getNombre())<=0)
			{
				aux=d;
			}

			if(pLista.next())
			{			
				pLista.avanzar();
			}
			else
			{
				break;
			}
		}
		return aux;
	}
	

	@Override //1B
	public LinkedList<Compania> darCompaniasTaxisInscritos() 
	{
		Lista<Compania> a = new Lista<Compania>();
		Lista<Compania> temp= listCompania;
		while(true)
		{
			Compania f =darMenor(temp);
			a.add(f);
			temp.delete(f);
			if(temp.size()==0)
				break;
		}
				//		listCompania.listing();
		//		while(true)
		//		{
		//			Compania d = listCompania.getCurrent();
		//			if(a.getElement(d)==null)
		//			{
		//				if(d.getNombre().compareTo(aux.getNombre())>=0)
		//				{
		//					sig=d;
		//				}
		//			}
		//			if(listCompania.size()==a.size())
		//			{
		//				break;
		//			}
		//			else
		//			{
		//				if(listCompania.next())
		//				{
		//					listCompania.avanzar();
		//				}
		//				else
		//				{
		//					listCompania.listing();
		//					a.add(sig);
		//				}
		//			}
		//		}
		//
		return a;
	}

	@Override //2B
	public Taxi darTaxiMayorFacturacion(RangoFechaHora rango, String nomCompania) 
	{
		String fechaFin= rango.getFechaFinal();
		String [] fechaFinal= fechaFin.split("T");
		String fechaIni= rango.getFechaInicial();
		String [] fechaInicial= fechaIni.split("T");
		listCompania.listing();
		Compania com = null;
		while(true)
		{
			Compania a = listCompania.getCurrent();
			if(a.getNombre().equalsIgnoreCase(nomCompania))
			{
				com=a;
				System.out.println(com.getNombre());
				break;
			}
			if(listCompania.next())
			{
				listCompania.avanzar();
			}
			else
			{
				break;
			}
		}

		double mayorFactDIne=0;
		double factTaxi=0;
		LinkedList<Taxi> tax =com.getTaxisInscritos();
		tax.listing();
		Taxi mayorFact=null;
		while(true)
		{
			Taxi a = tax.getCurrent();
			Lista<Service> b = a.getServicios();
			b.listing();
			boolean sale=false;
			while(!sale)
			{
				Service serv = b.getCurrent();
				if(fechaFinal[0].compareTo(rango.getFechaFinal())<=0 && fechaInicial[0].compareTo(rango.getFechaInicial()) >=0)
				{
					if(serv.getHoraFinal().compareTo(rango.getHoraFinal())<=0 && serv.getHoraInicial().compareTo(rango.getHoraInicio())>=0)
					{
						factTaxi+=serv.getTripTotal();
					}
				}
				if(b.next())
				{
					b.avanzar();
				}
				else
				{
					sale=true;
					if(factTaxi> mayorFactDIne)
					{
						mayorFact=a;
					}
				}

			}
			if(tax.next())
			{
				tax.avanzar();
			}
			else
			{
				break;
			}

		}

		System.out.println("Compa�ia: " + mayorFact.getCompany());
		System.out.println("Id: "+mayorFact.getTaxiId());
		return mayorFact;
	}

	@Override //3B
	public ServiciosValorPagado darServiciosZonaValorTotal(RangoFechaHora rango, String idZona)
	{
		ServiciosValorPagado f = new ServiciosValorPagado();
		Lista<Service> serviList= new Lista<Service>();
		String fechaFin= rango.getFechaFinal();
		String [] fechaFinal= fechaFin.split("T");
		String fechaIni= rango.getFechaInicial();
		String [] fechaInicial= fechaIni.split("T");
		Lista<Service> serviList2= new Lista<Service>();
		Lista<Service> serviList3= new Lista<Service>();	
		double totalMismaZona=0;
		double totalCOmenzaronZona=0;
		double totalTerminaronZona=0;
		listCompania.listing();
		while(true)
		{
			Compania a = listCompania.getCurrent();
			Lista<Taxi> b= a.getTaxisInscritos();
			b.listing();
			boolean siguiente = false;
			while(!siguiente)
			{
				Taxi c = b.getCurrent();
				boolean termino=false;
				Lista<Service> g = c.getServicios();
				g.listing();
				while(!termino)
				{
					Service serv = g.getCurrent();
					if(fechaFinal[0].compareTo(serv.getFechaFinal())<=0 && fechaInicial[0].compareTo(serv.getFechaInicial()) >=0)
					{
						if(rango.getHoraFinal().compareTo(serv.getHoraFinal())>=0 && rango.getHoraInicio().compareTo(serv.getHoraInicial())<=0)
						{
							if(Integer.parseInt(idZona)== serv.getPickupCommunityArea() && Integer.parseInt(idZona) ==serv.getDropoffCommunityArea())
							{

								serviList.add(serv);	
							}
							else if(Integer.parseInt(idZona)==serv.getPickupCommunityArea() )
							{

								serviList2.add(serv);				
							}
							else if(Integer.parseInt(idZona)==serv.getDropoffCommunityArea())
							{

								serviList3.add(serv);
							}
						}
					}
					if(g.next())
					{
						g.avanzar();
					}
					else
					{
						termino=true;
					}
				}
				if(b.next())
				{
					b.avanzar();
				}
				else
				{
					siguiente = true;
				}

			}
			if(listCompania.next())
			{
				listCompania.avanzar();
			}
			else
			{
				serviList.listing();
				boolean termino1=false;
				while(!termino1)
				{

					if(serviList.getActual()==null)
					{
						termino1=true;
					}
					else
					{
						Service aux =serviList.getCurrent();
						totalMismaZona+= aux.getTripTotal();
						if(serviList.next())
						{
							serviList.avanzar();
						}
						else
						{
							termino1=true;
						}
					}
				}

				serviList2.listing();
				boolean termino2=false;
				while(!termino2)
				{

					if (serviList2.getActual()==null)
					{
						termino2=true;
					}
					else
					{
						Service aux = serviList2.getCurrent();
						totalCOmenzaronZona+= aux.getTripTotal();
						if(serviList2.next())
						{
							serviList2.avanzar();
						}
						else
						{
							termino2=true;
						}
					}
				}
				serviList3.listing();
				boolean termino3=false;
				while(!termino3)
				{

					if(serviList3.getActual()==null)
					{
						termino3=true;
					}
					else
					{
						Service aux = serviList3.getCurrent();
						totalTerminaronZona+= aux.getTripTotal();

						if(serviList3.next())
						{
							serviList3.avanzar();
						}
						else
						{
							termino3=true;
						}
					}
				}
				f.setServiciosAsociados(serviList);
				System.out.println("n� zona: "+ idZona);
				System.out.println("n� de servicios que comenzaron y terminaron en esta zona: " + serviList.size());
				System.out.println("Costo total: " + totalMismaZona );
				System.out.println("n� de servicios que comenzaron: " + serviList2.size());
				System.out.println("Costo total: " + totalCOmenzaronZona );
				System.out.println("n� de servicios que terminaron: " + serviList3.size());
				System.out.println("Costo total: " + totalTerminaronZona );
				break;
			}

		}
		return f;
	}
	public ZonaServicios darMenorZona(Lista<ZonaServicios> pLista)
	{
		
		pLista.listing();
		ZonaServicios aux=pLista.getCurrent();
		while(true)
		{
			ZonaServicios d = pLista.getCurrent();
			if(d.getIdZona()<=aux.getIdZona())
			{
				aux=d;
			}

			if(pLista.next())
			{			
				pLista.avanzar();
			}
			else
			{
				break;
			}
		}
		return aux;
	}

	@Override //4B
	public LinkedList<ZonaServicios> darZonasServicios(RangoFechaHora rango)
	{
		Lista<ZonaServicios>f= listZonaServicios;
		Lista<ZonaServicios> b = new Lista<ZonaServicios>();
		f.listing();
		while(true)
		{
			ZonaServicios d = darMenorZona(f);
			f.delete(d);
			b.add(d);
			if(f.size()==0)
			{
				break;
			}
			else
			{
				f.avanzar();
			}
		}
		b.listing();
		while(true)
		{
			ZonaServicios a = b.getCurrent();
			String c=String.valueOf(a.getIdZona());
			darServiciosZonaValorTotal(rango, c);

			if(b.next())
			{
				b.avanzar();
			}
			else
			{
				break;
			}
		}

		return null;
	}

	@Override //2C
	public Lista<CompaniaServicios> companiasMasServicios(RangoFechaHora rango, int n)
	{

		String fechini= rango.getFechaInicial() ;
		String horaini= rango.getHoraInicio() ;
		String fechfin= rango.getFechaFinal() ;
		String horafin= rango.getHoraFinal() ;
		Lista<CompaniaServicios> retor = new Lista<CompaniaServicios>() ;
		Lista<CompaniaServicios> definitiva = new Lista<CompaniaServicios>() ;

		if (listCompania!=null)
		{
			listCompania.listing();
			//-------------------------------------------------------------------------------------

			while(listCompania.getActual() !=null)
			{

				Compania actual = listCompania.getCurrent();
				actual.getTaxisInscritos().listing();
				Lista<Service> listaDeServicios = new Lista<Service>() ;
				//-------------------------------------------------------------------------------------
				while(actual.getTaxisInscritos().next())
				{
					Taxi taxactual = actual.getTaxisInscritos().getCurrent() ;
					taxactual.getServicios().listing();
					//-------------------------------------------------------------------------------------

					while(taxactual.getServicios().next())
					{
						Service seractual = taxactual.getServicios().getCurrent() ;
						if(seractual.getFechaInicial().compareTo(fechini) >= 0 && seractual.getHoraInicial().compareTo(horaini) >= 0
								&& seractual.getFechaFinal().compareTo(fechfin) <= 0 && seractual.getHoraFinal().compareTo(horafin) <= 0)
						{

							listaDeServicios.add(seractual);

						}
						taxactual.getServicios().avanzar();
					}
					actual.getTaxisInscritos().avanzar();
				}

				CompaniaServicios nueva = new CompaniaServicios(actual.getNombre(), listaDeServicios) ;

				if(retor.getElement(nueva)!=null)
				{
					retor.getElement(nueva).getServicios().combinarLista(listaDeServicios);
				}
				else
				{
					retor.add(nueva);
				}

				listCompania.avanzar();
			}

		}
		//		Teniendo la lista con todos los servicios que est�n el rango ser agregan a la lista que si se va a retornar con un tama�o dado por par�metro
		while(definitiva.size()<n)
		{
			retor.listing();
			CompaniaServicios laMas = retor.getCabeza().darT() ;
			while(true)
			{

				if(retor.getCurrent().getServicios().size() > laMas.getServicios().size() )
				{

					laMas= retor.getCurrent() ;
				}
				retor.avanzar();
				if(retor.next()==false)
				{
					break ;
				}

			}

			definitiva.add(laMas);
			retor.delete(laMas);
		}

		return definitiva;
	}

	@Override //3C
	public Lista<CompaniaTaxi> taxisMasRentables()
	{
		Lista<CompaniaTaxi> lista = new Lista<CompaniaTaxi>();
		listCompania.listing();
		while(true)
		{
			Compania a = listCompania.getCurrent();
			Lista<Taxi> b = a.getTaxisInscritos();
			Taxi masRentable= null;
			double montoRentable=0;
			boolean siguTaxi=false;
			b.listing();
			while(!siguTaxi)
			{
				double dinero=0;
				double distancia=0;
				Taxi act= b.getCurrent();
				Lista<Service> listServi= act.getServicios();
				listServi.listing();
				boolean siguServi=false;
				while(!siguServi)
				{
					Service serv = listServi.getCurrent();
					dinero+=serv.getTripTotal();
					distancia+=serv.getTripMiles();
					if(listServi.next())
					{
						listServi.avanzar();
					}
					else
					{
						siguServi=true;
					}
				}
				double rentabilidad= dinero/distancia;
				if(rentabilidad> montoRentable)
				{
					montoRentable=rentabilidad;
					masRentable=act;
				}
				if(b.next())
				{

					b.avanzar();
				}
				else
				{
					siguTaxi=true;
				}


			}

			if(listCompania.next())
			{

				listCompania.avanzar();

				CompaniaTaxi agregar = new CompaniaTaxi();
				agregar.setNomCompania(a.getNombre());
				agregar.setTaxi(masRentable);
				lista.add(agregar);
			}
			else
			{
				break;
			}
		}

		return lista;
	}

	@Override //4C
	public IStack <Service> darServicioResumen(String taxiId, String horaInicial, String horaFinal, String fecha) 
	{
		Pila<Service> resu = new Pila<Service>();
		Taxi seleccion= null;
		listCompania.listing();
		while(true)
		{
			Compania a = listCompania.getCurrent();
			Lista<Taxi> b= a.getTaxisInscritos();
			b.listing();
			boolean siguiente = false;
			while(!siguiente)
			{
				Taxi c = b.getCurrent();
				if(c.getTaxiId().equals(taxiId))
				{
					seleccion=c;
					break;
				}
				if(b.next())
				{
					b.avanzar();
				}
				else
				{
					siguiente = true;
				}
			}
			if(listCompania.next())
			{
				listCompania.avanzar();
			}
			else
			{
				break;
			}

		}

		Lista<Service> d = seleccion.getServicios();
		d.listing();
		while(true)
		{
			Service ser=d.getCurrent();

			if(ser.getHoraInicial().compareTo(horaInicial)>=0 )
			{
				if(ser.getHoraFinal().compareTo(horaFinal)<=0 )
				{

					if(ser.getFechaInicial().compareTo(fecha)>=0)
					{

						System.out.println(ser.getTripId());
						resu.push(ser);

					}
				}
			}
			if(d.next())
			{
				d.avanzar();
			}
			else 
				break;
		}
		return resu;
	}
	public Lista<Compania> getListCompany()
	{
		return listCompania ;
	}



}
